package softplan.boliche;

import java.util.ArrayList;
import java.util.List;

public class Rodada {

	private List<Lancamento> lancamentos = new ArrayList<>();

	private int pontosDaRodada;

	public List<Lancamento> getLancamentos() {
		return lancamentos;
	}

	public void setLancamentos(List<Lancamento> lancamentos) {
		this.lancamentos = lancamentos;
	}

	public int getPontosDaRodada() {
		return pontosDaRodada;
	}

	public void setPontosDaRodada(int pontosDaRodada) {
		this.pontosDaRodada = pontosDaRodada;
	}

}