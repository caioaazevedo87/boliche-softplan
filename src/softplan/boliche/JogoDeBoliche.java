package softplan.boliche;

public interface JogoDeBoliche {

	public void processarLancamento(int pinosDerrubanos);

	public int calcularPlacar();
	
	public void iniciarJogo();

	public void finalizarJogo();

}