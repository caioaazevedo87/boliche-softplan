package softplan.boliche;

public class SimularJogo {
	
	private JogoDeBoliche jogoDeBoliche = new JogoDeBolicheImpl();
	
	public int simularJogo(int[] jogadas) {
		jogoDeBoliche.iniciarJogo();
		
		for (int jogada : jogadas) {
			jogoDeBoliche.processarLancamento(jogada);
		}

		jogoDeBoliche.finalizarJogo();
		
		return jogoDeBoliche.calcularPlacar();
	}

	public JogoDeBoliche getJogoDeBoliche() {
		return jogoDeBoliche;
	}

	public void setJogoDeBoliche(JogoDeBoliche jogoDeBoliche) {
		this.jogoDeBoliche = jogoDeBoliche;
	}

}
