package softplan.boliche;

public class Lancamento {

    private int pinosDerrubados;

    private boolean strike;

    private boolean spare;
    

	public int getPinosDerrubados() {
		return pinosDerrubados;
	}

	public void setPinosDerrubados(int pinosDerrubados) {
		this.pinosDerrubados = pinosDerrubados;
	}

	public boolean isStrike() {
		return strike;
	}

	public void setStrike(boolean strike) {
		this.strike = strike;
	}

	public boolean isSpare() {
		return spare;
	}

	public void setSpare(boolean spare) {
		this.spare = spare;
	}
    
    

}