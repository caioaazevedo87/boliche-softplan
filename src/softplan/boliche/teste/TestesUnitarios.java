package softplan.boliche.teste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import softplan.boliche.SimularJogo;

class TestesUnitarios {

	@Test
	void jogo1() {
		int[] jogo1 = new int[] { 1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3, 6, 4, 10, 2, 8, 6 }; // Placar esperado: 133
		int placarEsperado = 133;
		
		SimularJogo s = new SimularJogo();
		int resultado = s.simularJogo(jogo1);
		assertEquals(placarEsperado, resultado);
	}

	@Test
	void jogo2() {
		int[] jogo2 = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2, 3, 0, 0 }; // Placar esperado: 20
		int placarEsperado = 20;

		SimularJogo s = new SimularJogo();
		int resultado = s.simularJogo(jogo2);
		assertEquals(placarEsperado, resultado);
	}

	@Test
	void jogo3() {
		int[] jogo3 = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 8, 2, 3, 0, 0 }; // Placar esperado: 17
		int placarEsperado = 17;
		
		SimularJogo s = new SimularJogo();
		int resultado = s.simularJogo(jogo3);
		assertEquals(placarEsperado, resultado);
	}

	@Test
	void jogo4() {
		int[] jogo4 = new int[] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 }; // Placar esperado: 300
		int placarEsperado = 300;
		
		SimularJogo s = new SimularJogo();
		int resultado = s.simularJogo(jogo4);
		assertEquals(placarEsperado, resultado);
	}

}
