package softplan.boliche;

import static softplan.boliche.Constantes.QUANTIDADE_DE_PINOS;
import static softplan.boliche.Constantes.QUANTIDADE_DE_RODADAS_POR_PARTIDA;

import java.util.ArrayList;
import java.util.List;

public class JogoDeBolicheImpl implements JogoDeBoliche {

	List<Rodada> rodadas = new ArrayList<Rodada>();

	@Override
	public void processarLancamento(int pinosDerrubanos) {
		System.out.println("Rodada " + capturarNumeroDaRodada() + ", Lancamento " + capturarNumeroDoLancamento()
				+ ", Pinos Derrubados: " + pinosDerrubanos);
		somarPlacarEspecialParaARodadaAnterior(pinosDerrubanos);
		rodadaAtual().setPontosDaRodada(rodadaAtual().getPontosDaRodada() + pinosDerrubanos);

		if (isPrimeiroLancamentoDaRodada()) {
			rodadaAtual().getLancamentos().add(new Lancamento());
			rodadaAtual().getLancamentos().get(0).setPinosDerrubados(pinosDerrubanos);
			rodadaAtual().getLancamentos().get(0).setStrike(this.isStrike(pinosDerrubanos));
			exibirPlacar();
			if (this.isStrike(pinosDerrubanos) && capturarNumeroDaRodada() < 10) {
				avancarParaAProximaRodada();
			}
		} else {
			rodadaAtual().getLancamentos().add(new Lancamento());
			rodadaAtual().getLancamentos().get(1).setPinosDerrubados(pinosDerrubanos);
			rodadaAtual().getLancamentos().get(1).setSpare(this.isSpare(pinosDerrubanos));
			exibirPlacar();
			avancarParaAProximaRodada();
		}
	}

	@Override
	public int calcularPlacar() {
		int placarDoJogo = 0;
		for (int i = 0; i < rodadas.size(); i++) {
			placarDoJogo += rodadas.get(i).getPontosDaRodada();
		}

		return placarDoJogo;
	}

	@Override
	public void iniciarJogo() {
		avancarParaAProximaRodada();
	}

	@Override
	public void finalizarJogo() {
		System.out.println("Jogo Finalizado");
	}

	private void somarPlacarEspecialParaARodadaAnterior(int pinosDerrubanos) {
		if (isUltimaRodada() && capturarNumeroDoLancamento() == 3) {
			return;
		}

		if (rodadas.size() > 1) {
			if ((capturarRodadaAnterior().getLancamentos().get(0).isStrike()
					|| (capturarRodadaAnterior().getLancamentos().get(1).isSpare()
							&& isPrimeiroLancamentoDaRodada()))) {
				rodadaAtual().setPontosDaRodada(rodadaAtual().getPontosDaRodada() + pinosDerrubanos);
			}

		}
		if (rodadas.size() > 2) {
			if (capturarRodadaAnteriorDaAnterior().getLancamentos().get(0).isStrike()
					&& capturarRodadaAnterior().getLancamentos().size() < 2 && isPrimeiroLancamentoDaRodada()) {
				rodadaAtual().setPontosDaRodada(rodadaAtual().getPontosDaRodada() + pinosDerrubanos);
			}
		}

	}

	private Rodada rodadaAtual() {
		Rodada rodadaAtual = rodadas.get(rodadas.size() - 1);
		return rodadaAtual;
	}

	private boolean isUltimaRodada() {
		return capturarNumeroDaRodada() == 10;
	}

	private int capturarNumeroDaRodada() {
		return rodadas.size();
	}

	private int capturarNumeroDoLancamento() {
		return rodadas.get(rodadas.size() - 1).getLancamentos().size() + 1;
	}

	private Rodada capturarRodadaAnterior() {
		return rodadas.get(rodadas.size() - 2);
	}

	private Rodada capturarRodadaAnteriorDaAnterior() {
		return rodadas.get(rodadas.size() - 3);
	}

	private boolean isPrimeiroLancamentoDaRodada() {
		return rodadaAtual().getLancamentos().isEmpty();
	}

	private boolean isSpare(int pinosDerrubanos) {
		return rodadaAtual().getLancamentos().get(0).getPinosDerrubados() + pinosDerrubanos == QUANTIDADE_DE_PINOS;
	}

	private boolean isStrike(int pinosDerrubanos) {
		return pinosDerrubanos == QUANTIDADE_DE_PINOS;
	}

	private void avancarParaAProximaRodada() {
		if (rodadas.size() < QUANTIDADE_DE_RODADAS_POR_PARTIDA) {
			Rodada novaRodada = new Rodada();
			rodadas.add(novaRodada);
		}
	}

	private void exibirPlacar() {
		System.out.println("Placar: " + calcularPlacar());
	}
}