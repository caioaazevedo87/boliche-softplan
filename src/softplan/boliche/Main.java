package softplan.boliche;

public class Main {

	public static final int[] JOGO1 = new int[] { 1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3, 6, 4, 10, 2, 8, 6 }; //Placar esperado: 133
	public static final int[] JOGO2 = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2, 3, 0, 0 }; //Placar esperado: 20
	public static final int[] JOGO3 = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 8, 2, 3, 0, 0 }; //Placar esperado: 17
	public static final int[] JOGO4 = new int[] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 }; //Placar esperado: 300

	public static void main(String[] args) {
		int resultado = new SimularJogo().simularJogo(JOGO1);
		System.out.println(resultado);
	}
}
